import unittest
from unittest.mock import Mock, patch, call
from utils import ped_parsing
from opencga_tools import ped_file


class NamedMock(Mock):
    @property
    def name(self):
        return self._name


class TestClient(unittest.TestCase):
    def setUp(self):
        with patch('opencga_tools.ped_file.OpencgaClient'),\
             patch('opencga_tools.ped_file.ClientConfiguration'),\
             patch('opencga_tools.ped_file.PedFile'):
            self.client = ped_file.Client(
                Mock(
                    ped_file='a_ped_file', study='a_study', host='a_host', user='a_user', password='a_password',
                    disorder_id=None, sample_file=None
                )
            )

        self.m = NamedMock(_name='m', sex='FEMALE', mother=None, father=None, phenotype='1')
        self.f = NamedMock(_name='f', sex='MALE', mother=None, father=None, phenotype='1')
        self.i = NamedMock(_name='i', sex='FEMALE', mother=self.m, father=self.f, phenotype='2')

        self.fake_individual_search_results = [
            Mock(response=[{'results': [{'sex': 'FEMALE', 'mother': {}, 'father': {}, 'samples': [{'id': 'm'}]}]}]),
            Mock(response=[{'results': [{'sex': 'MALE', 'mother': {}, 'father': {}, 'samples': [{'id': 'f'}]}]}]),
            Mock(response=[{'results': [
                {'sex': 'FEMALE', 'mother': {'id': 'm'}, 'father': {'id': 'f'}, 'samples': [{'id': 'i'}]}
            ]}])
        ]

    def test_add_or_update_individual_new(self):
        self.client.client.individuals.search.return_value.get_num_results.return_value = 0
        self.client._add_or_update_individual(self.i)
        self.client.client.individuals.create.assert_has_calls(
            (
                call({'name': 'm', 'sex': 'FEMALE'}, samples=['m'], study='a_study'),
                call({'name': 'f', 'sex': 'MALE'}, samples=['f'], study='a_study'),
                call({'name': 'i', 'sex': 'FEMALE', 'mother': 'm', 'father': 'f'}, samples=['i'], study='a_study')
            )
        )
        self.client.client.reset_mock()

        # disorder to add
        self.client.disorder = {'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}
        self.client._add_or_update_individual(self.i)
        self.client.client.individuals.create.assert_has_calls(
            (
                call({'name': 'm', 'sex': 'FEMALE'}, samples=['m'], study='a_study'),
                call({'name': 'f', 'sex': 'MALE'}, samples=['f'], study='a_study'),
                call(
                    {
                        'name': 'i',
                        'sex': 'FEMALE',
                        'mother': 'm',
                        'father': 'f',
                        'disorders': [{'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}]
                    },
                    samples=['i'],
                    study='a_study'
                )
            )
        )

    def test_add_or_update_individual_data_present(self):
        # no new data
        self.client.disorder = {'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}
        self.client.client.individuals.search.return_value.get_num_results.return_value = 1
        search_results = self.fake_individual_search_results.copy()
        search_results[-1].response[0]['results'][0]['disorders'] = [{'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}]
        self.client.client.individuals.search.side_effect = search_results
        self.client._add_or_update_individual(self.i)
        self.client.client.individuals.create.assert_not_called()
        self.client.client.individuals.update.assert_not_called()

    def test_add_or_update_individual_update(self):
        self.client.disorder = {'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}
        search_results = self.fake_individual_search_results.copy()
        search_results[-1].response[0]['results'][0]['disorders'] = [
            {'id': 'HPO:01235', 'name': 'Another disease', 'source': 'HPO'}
        ]
        self.client.client.individuals.search.side_effect = (
            Mock(response=[{'results': [{'sex': 'FEMALE', 'mother': {}, 'father': {}, 'samples': [{'id': 'm'}]}]}]),
            Mock(response=[{'results': [{'sex': 'MALE', 'mother': {}, 'father': {}, 'samples': [{'id': 'f'}]}]}]),
            Mock(response=[{'results': [
                {
                    'sex': 'FEMALE',
                    'mother': {'id': 'f'},  # parents wrong way around
                    'father': {'id': 'm'},
                    'samples': [{'id': 'i'}],
                    'disorders': [{'id': 'HPO:01235', 'name': 'Some other disease', 'source': 'HPO'}]  # pre-existing disorder
                }
            ]}])
        )

        self.client._add_or_update_individual(self.i)
        self.client.client.individuals.update.assert_called_once_with(
            'i',
            {
                'name': 'i',
                'sex': 'FEMALE',
                'mother': 'm',  # correcting parents
                'father': 'f',
                'samples': ['i'],
                'disorders': [
                    {'id': 'HPO:01235', 'name': 'Some other disease', 'source': 'HPO'},
                    {'id': 'HPO:01234', 'name': 'A disease', 'source': 'HPO'}
                ]
            }
        )

    def test_add_or_update_family(self):
        f = NamedMock(_name='f1', members={'this': NamedMock(_name='this'), 'that': NamedMock(_name='that')})

        # family doesn't exist, adding as new
        self.client.client.families.search.return_value.get_num_results.return_value = 0
        self.client._add_or_update_family(f)
        self.client.client.families.search.assert_called_with(study='a_study', name='f1')
        self.client.client.families.create.assert_called_with({'name': 'f1'}, study='a_study', members=['that', 'this'])
        self.client.client.reset_mock()

        # family exists, updating members
        self.client.client.families.search.return_value.get_num_results.return_value = 1
        self.client.client.families.search.return_value.response = [{'results': [{'members': [{'name': 'this'}, {'name': 'that'}]}]}]
        self.client._add_or_update_family(f)
        self.client.client.families.search.assert_called_with(study='a_study', name='f1')
        self.client.client.families.create.assert_not_called()
        self.client.client.families.update.assert_not_called()
        f.members['other'] = NamedMock(_name='other')
        self.client._add_or_update_family(f)
        self.client.client.families.update.assert_called_with('f1', {'members': ['other', 'that', 'this']}, study='a_study')

        # bad things are happening
        self.client.client.families.search.return_value.get_num_results.return_value = 2
        with self.assertRaises(ValueError):
            self.client._add_or_update_family(f)

    @patch.object(ped_file.Client, '_add_or_update_family')
    @patch.object(ped_file.Client, '_add_or_update_individual')
    def test_upload_family_data(self, patched_individual, patched_family):
        family = ped_parsing.Family(
            'f1',
            x=ped_parsing.Individual('f1', 'x', 'father', 'mother', '2', '1'),
            mother=ped_parsing.Individual('f1', 'mother', '0', '0', '2', '1'),
            father=ped_parsing.Individual('f1', 'father', '0', 'grandmother', '1', '1'),
            grandmother=ped_parsing.Individual('f1', 'grandmother', '0', '0', '2', '1')
        )
        family.resolve_relationships()
        self.client.samples_by_individual = {
            'x': ['x_normal', 'x_tumour']
        }
        self.client._upload_family_data(family)
        patched_family.assert_called_once_with(family)

        exp_calls = [
            call(family.members['x']),
            call(family.members['mother']),
            call(family.members['father']),
            call(family.members['grandmother'])
        ]

        patched_individual.assert_has_calls(exp_calls)
        self.assertEqual(patched_individual.call_count, 4)

    @patch('builtins.open')
    def test_parse_sample_file(self, patched_open):
        patched_open.return_value.__enter__.return_value = (
            'ind1,sample1',
            'ind2,sample2,sample3',
            'ind3'
        )

        self.assertDictEqual(self.client.parse_sample_file(None), {})
        self.assertDictEqual(
            self.client.parse_sample_file('a_sample_file'),
            {
                'ind1': ['sample1'],
                'ind2': ['sample2', 'sample3'],
                'ind3': []
            }
        )
