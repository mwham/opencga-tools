import logging
from utils.ped_parsing import PedFile
from pyopencga.opencga_client import OpencgaClient
from pyopencga.opencga_config import ClientConfiguration

description = 'Update family relationship data from a Ped file'
arguments = (
    ('--ped_file',        {'help': 'Ped file to upload family data for'}),
    ('--disorder_id',     {'help': "ID (e.g. HPO term) for the disease the Ped file's 'affected' column applies to.", 'default': None}),
    ('--disorder_name',   {'help': 'Disorder descriptive name', 'default': None}),
    ('--disorder_source', {'help': "Source for the disease ID if it's an ontology, eg. HPO, OMIM", 'default': None}),
    ('--sample_file',     {'help': 'CSV linking individuals to samples, in the format `ind1,sample1,sample2,...', 'default': None}),
    ('--study',           {'help': 'OpenCGA study that the samples are part of'}),
    ('--host',            {'help': 'The OpenCGA server to interact with, e.g. https://opencga-instance.ed.ac.uk/opencga-2.0.0'}),
    ('--user',            {'help': 'User to connect as'}),
    ('--password',        {'help': 'OpenCGA password (optional - supplied in stdin otherwise)', 'default': None})
)

logger = logging.getLogger('ped_file_upload')


class Client:
    """Connects to an OpenCGA 2.0.0 instance and uploads sample metadata from a Ped file."""
    def __init__(self, args):
        self.study = args.study

        if args.disorder_id:
            self.disorder = {'id': args.disorder_id}
            if args.disorder_name:
                self.disorder['name'] = args.disorder_name
            if args.disorder_source:
                self.disorder['source'] = args.disorder_source
        else:
            self.disorder = None

        self.ped_file = PedFile(args.ped_file)
        self.ped_file.add_individuals()
        self.ped_file.resolve_relationships()

        self.client = OpencgaClient(ClientConfiguration({'rest': {'host': args.host}}))
        self.client.login(args.user, args.password)
        self.samples_by_individual = self.parse_sample_file(args.sample_file)

    @staticmethod
    def parse_sample_file(sample_file):
        individuals = {}
        if not sample_file:
            return individuals

        with open(sample_file) as f:
            for line in f:
                individual, *samples = line.strip().split(',')
                individuals[individual] = sorted(samples)

        return individuals

    def upload_ped_data(self):
        logger.info('Uploading from %s', self.ped_file.ped_file)
        for family in self.ped_file.families.values():
            self._upload_family_data(family)

        logger.info('Done')

    def _upload_family_data(self, family):
        """
        Resolve a payload for updating families endpoint. The server handles creating individuals in the right order so
        we can just upload everything at once, but there's no ability to link samples as part of that - so we do that
        ourselves. See Swagger docs on IndividualPOST at /opencga-<version>/webservices/#!/Families/updateByPost_1.
        """

        for individual in family.members.values():
            self._add_or_update_individual(individual)

        self._add_or_update_family(family)

    def _add_or_update_individual(self, individual):
        if individual.mother:
            self._add_or_update_individual(individual.mother)

        if individual.father:
            self._add_or_update_individual(individual.father)

        payload = {
            'name': individual.name,
            'sex': individual.sex
        }
        samples = self.samples_by_individual.get(individual.name, [individual.name])

        if individual.mother:
            payload['mother'] = individual.mother.name

        if individual.father:
            payload['father'] = individual.father.name

        individual_affected = individual.phenotype == '2'  # OBSERVED

        response = self.client.individuals.search(study=self.study, name=individual.name)
        if response.get_num_results() == 0:
            if self.disorder and individual_affected:
                payload['disorders'] = [self.disorder]

            self.client.individuals.create(payload, study=self.study, samples=samples)
        else:
            existing_data = response.response[0]['results'][0]
            existing_disorders = existing_data.get('disorders', [])
            payload['samples'] = samples
            differences = (
                payload['sex'] != existing_data['sex'],
                payload.get('mother') != existing_data['mother'].get('id'),
                payload.get('father') != existing_data['father'].get('id'),
                samples != sorted(s['id'] for s in existing_data['samples']),
                self.disorder and individual_affected and self.disorder not in existing_disorders
            )
            if any(differences):
                logger.info('Updating information for individual %s', individual.name)
                if self.disorder and individual_affected:
                    payload['disorders'] = existing_disorders
                    for d in payload['disorders']:
                        if self.disorder['id'] == d['id']:  # modifying existing disorder
                            d.update(self.disorder)
                    else:
                        payload['disorders'].append(self.disorder)  # new disorder

                self.client.individuals.update(individual.name, payload)

    def _add_or_update_family(self, family):
        """Add family data to OpenCGA."""

        response = self.client.families.search(study=self.study, name=family.name)
        nfams = response.get_num_results()
        new_members = sorted(family.members)

        if nfams == 0:
            logger.info('Pushing new family %s', family.name)
            return self.client.families.create({'name': family.name}, study=self.study, members=new_members)

        elif nfams == 1:
            existing_members = sorted(m['name'] for m in response.response[0]['results'][0]['members'])
            if existing_members != new_members:
                logger.info(
                    'Updating members for family %s from %s to %s',
                    family.name,
                    existing_members,
                    new_members
                )
                self.client.families.update(family.name, {'members': new_members}, study=self.study)
            else:
                logger.info('No updates to make for family %s', family.name)

        else:
            raise ValueError('%i families found for %s' % (nfams, family.name))


def main(args):
    formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s] %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    c = Client(args)
    c.upload_ped_data()
