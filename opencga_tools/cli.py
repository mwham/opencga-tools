import argparse
from opencga_tools import ped_file


def main(argv=None):
    a = argparse.ArgumentParser()
    subparsers = a.add_subparsers()

    for module in (ped_file,):
        # the modules here are called 'opencga_tools.x', so trim it to just 'x'
        sub = subparsers.add_parser(module.__name__.split('.')[-1], help=module.description)
        for args, kwargs in module.arguments:
            if type(args) is str:
                args = (args,)

            sub.add_argument(*args, **kwargs)

        sub.set_defaults(func=module.main)

    args = a.parse_args(argv)
    return args.func(args)
