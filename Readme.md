# opencga_admin

This is a Python library consisting of automation utilities for interacting with an OpenCGA instance. Current
submodules:

- ped_file: Uploading of family information to samples from a Ped file

## Installation

This module can be installed via Pipe into a Conda environment or Python venv from the Git repository:

    pip install git+https://git.ecdf.ed.ac.uk/mwham/opencga-tools.git


## ped_file

This is a utility for uploading family data and associating related samples with each other. OpenCGA has Family
entities, which contain Individuals, which can have parent, sibling and sample information (one Individual can have
multiple samples). This assumes that samples have already been loaded in, and by default will look to associate an
individual with one sample of the same name.

This module can be called with:

    opencga.py ped_file
      --user <user> --host <opencga_server>/opencga-2.0.0
      --study <study_id> --ped_file <ped_file>
      --disorder_id <disease_id> --disorder_name <disease_name> --disorder_source <disease_source> 

You will be prompted for your OpenCGA password, and the script will load and validate the Ped file (raising any known
errors), and make any updates needed.

Given the following file:

    Fam1 person1 person2 person3 1 2
    Fam1 person2 0 0 1 1
    Fam1 person3 0 0 2 1

OpenCGA will be updated with the following information:

    family:
      name: Fam1
      individuals:
        - name: person1
          father: person2
          mother: person3
          samples: ['person1']
        - name: person2
          samples: ['person2']
        - name: person3
          samples: ['person3']


### Samples file

In the case of multiple samples per individual or where different names are wanted in the Individual and sample, it's
possible to supply a CSV mapping desired individual names to samples:

    person1,sample1_normal,sample1_tumour
    person2,sample2
    person3,sample3
    
The first column should be the individual name, and any columns further to the right will be counted as samples to
associate. Any individuals not found in the CSV will be given the default behaviour of one sample with the same name as
the individual.

The CSV and Ped file above would give the following data:

    family:
      name: Fam1
      individuals:
        - name: person1
          father: person2
          mother: person3
          samples: ['person1_normal', 'person1_tunour']
        - name: person2
          samples: ['sample2']
        - name: person3
          samples: ['sample3']


### Specifying disorders

The Ped file can be associated with a specific disease by specifying the command line arguments `--disorder_id`,
`--disorder_name` and `--disorder_source`. These can be freeform text, but the ID and source are intended to be
ontologies, e.g. disease term `HPO:01234` from the source `HPO`. This information will be added to the field `disorders`
in the Individual.

If an individual already has disorders associated, then this script will look at the disorder ID being added. If the ID
is not yet present in individual.disorders, the new disorder will be appended. If it is already present, then the name
and source for that disorder will be updated.
