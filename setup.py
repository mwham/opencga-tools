import setuptools
from glob import glob
from opencga_tools import __version__

with open('Readme.md') as f:
    long_description = f.read()

with open('requirements.txt') as f:
    requirements = [line.strip() for line in f if line and not line.startswith('#')]

setuptools.setup(
    name='OpenCGA-Tools',
    version=__version__,
    description='Utilities for interacting with an OpenCGA 2.0.0 instance.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Murray Wham',
    author_email='murray.wham@ed.ac.uk',
    keywords='opencga api',
    install_requires=requirements,
    packages=['opencga_tools'],
    scripts=glob('bin/*.py')
)
